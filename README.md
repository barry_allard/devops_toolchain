# devops_toolchain

## A set of UNIX-philosophy tools to make devops more like a vacation

## Installation

### Individually
    go get -u github.com/steakknife/devops_toolchain/lib
    go get -u github.com/steakknife/devops_toolchain/<tool>
    go build  github.com/steakknife/devops_toolchain/<tool>
    # <tool> bin built in cwd

### All
    curl -L https://gist.githubusercontent.com/steakknife/<TBD>/raw/install_full_devops_toolchain.sh | sh

## Tools

### setuser

Correctly set `setuid()`, `getgid()` and env vars `USER`, `HOME`, `GID`, `UID`

### waitport

Wait for a TCP or UDP port to be opened on the local system

### waitremoteport

Wait for a TCP port to be opened on a remote system

## Supported platforms

- FreeBSD
- OpenBSD
- NetBSD
- Linux
- Solaris
- OSX

## Todo

- test suite (needs root)

## License

MIT

## Authors

Barry Allard

## Contribs

Patches welcome!
