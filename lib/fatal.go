package lib

import (
  "fmt"
  "os"
)

func Fatal(args ...interface{}) {
  fmt.Fprintln(os.Stderr, " ! ", fmt.Sprint(args...))
  os.Exit(1)
}
