package lib

import (
  "flag"
  "fmt"
  "os"
)

var d bool = false
var v bool = false
var q bool = false

func LogFlags() {
  flag.BoolVar(&d, "d", false, "Debug output")
  flag.BoolVar(&v, "v", false, "Verbose output")
  flag.BoolVar(&q, "q", false, "Silence (no output)")
}

func Debug(args ...interface{}) {
  if d {
    Stdout(args...)
  }
}

func Verbose(args ...interface{}) {
  if v {
    Stdout(args...)
  }
}

func Stdout(args ...interface{}) {
  if !q {
    fmt.Println(args...)
  }
}

func Stderr(args ...interface{}) {
  if !q {
    fmt.Fprintln(os.Stderr, args...)
  }
}
