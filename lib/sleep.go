package lib

import (
  "fmt"
  "math"
  "time"
)

const Indefinitely = -1.0
const forever = math.MaxUint32 * time.Second

/* timeout is in seconds */
func Sleep(timeout float64) {
  duration, _ := time.ParseDuration(fmt.Sprintf("%fs", timeout))
  if timeout == Indefinitely {
    duration = forever
  }
  time.Sleep(duration)
}
