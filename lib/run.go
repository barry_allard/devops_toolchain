package lib

/* returns true if each() returns nil and timeout did not occur */
func Run(before func(), each func() error, timeout float64) bool {
  if before != nil {
    before()
  }

  ch := make(chan bool, 1)
  err_ch := make(chan error, 1)
  var err error

  go func() {
    Sleep(timeout)
    ch <- false
  }()

  go func() {
    Debug("Run(): blocking for each()")
    var err2 error
    err2 = each()
    err_ch <- err2
    Debug("Run(): each() returned", err2)
  }()

  for {
    select {
    case <-ch:
      Debug("Run(): timeout")
      return false
    case err = <-err_ch:
      if err == nil {
        return true
      }
    }
  }
}
