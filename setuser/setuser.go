package main

import (
  "flag"
  "fmt"
  "github.com/steakknife/devops_toolchain/lib"
  "os"
  "os/user"
  "strconv"
  "syscall"
)

func main() {

  lib.LogFlags()

  flag.Usage = func() {
    fmt.Fprintf(os.Stderr, "setuser [options] username command [args...]")
    flag.PrintDefaults()
  }

  flag.Parse()

  argv := flag.Args()

  if len(argv) < 2 {
    flag.Usage()
    os.Exit(1)
  }

  username := argv[0]
  user, err := user.Lookup(username)
  if err != nil {
    lib.Fatal(fmt.Sprintf("user %s not found", username))
  }

  gid, _ := strconv.Atoi(user.Gid)
  if syscall.Setgid(gid) != nil {
    lib.Fatal("cannot setgid(): which means: cannot alter this user's gid.  did you run as root?")
  }

  uid, _ := strconv.Atoi(user.Uid)
  if syscall.Setuid(uid) != nil {
    lib.Fatal("cannot setuid()")
  }

  if os.Setenv("USER", username) != nil {
    lib.Fatal("cannot setenv USER")
  }

  if os.Setenv("HOME", user.HomeDir) != nil {
    lib.Fatal("cannot setenv HOME")
  }

  if os.Setenv("UID", user.Uid) != nil {
    lib.Fatal("cannot setenv UID")
  }

  if os.Setenv("GID", user.Gid) != nil {
    lib.Fatal("cannot setenv GID")
  }

  command := argv[1]
  args := argv[1:]
  err = syscall.Exec(command, args, os.Environ()) // should not return
  lib.Fatal("cannot execvp()")
}
