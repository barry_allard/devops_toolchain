package main

import (
  "flag"
  "fmt"
  "github.com/steakknife/devops_toolchain/lib"
  "net"
  "os"
  "strconv"
)

const DefaultTimeout = 3.0 // seconds

func main() {
  var ip4, ip6 bool
  var w float64
  var n int64

  flag.BoolVar(&ip4, "4", false, "IPv4 only")
  flag.BoolVar(&ip6, "6", false, "IPv6 only")
  flag.Float64Var(&w, "w", DefaultTimeout, "Delay (defaults to 1.0s)")
  flag.Int64Var(&n, "n", -1, "Number of pings")
  lib.LogFlags()

  flag.Usage = func() {
    fmt.Fprintf(os.Stderr, "portping [options] host:port [timeout]")
    flag.PrintDefaults()
  }

  flag.Parse()

  argv := flag.Args()

  if len(argv) < 1 {
    flag.Usage()
    os.Exit(1)
  }

  address := argv[0]

  timeout := DefaultTimeout
  if len(argv) >= 2 {
    timeout, err := strconv.ParseFloat(argv[1], 64)
    if err != nil || timeout < 0 {
      lib.Fatal(fmt.Sprintf("Timeout %s bad", argv[1]))
    }
  }

  var proto string
  if ip4 && !ip6 {
    proto = "tcp4"
  } else if !ip4 && ip6 {
    proto = "tcp6"
  } else {
    proto = "tcp"
  }

  for ; n != 0; n-- {

    if lib.Run(nil, /* before */
      func() error { // each
        _, err := net.Dial(proto, address)
        return err
      }, timeout) {
      lib.Verbose("Connected to", address, "success!")
    } else {
      lib.Stdout("Timeout connecting to ", address)
    }

    lib.Sleep(w)
  }
}
