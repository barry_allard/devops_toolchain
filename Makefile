SUBDIRS=$(shell find . -type d | egrep -v '^(\./(lib|bin|\.git)|\.$$)')
TARGETS=build clean

$(TARGETS): $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) $(MAKEFLAGS) -C $@ $(MAKECMDGOALS)

.PHONY: $(SUBDIRS)
