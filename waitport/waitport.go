package main

import (
  "flag"
  "fmt"
  "github.com/steakknife/devops_toolchain/lib"
  "net"
  "os"
  "os/exec"
  "strconv"
)

func main() {
  var ip4, ip6, udp bool

  flag.BoolVar(&ip4, "4", false, "IPv4 only")
  flag.BoolVar(&ip6, "6", false, "IPv6 only")
  flag.BoolVar(&udp, "u", false, "UDP instead of TCP")
  lib.LogFlags()

  flag.Usage = func() {
    fmt.Fprintf(os.Stderr, "waitport [options] [-4] [-6] [-u] port [timeout]")
    flag.PrintDefaults()
  }

  flag.Parse()

  argv := flag.Args()

  if len(argv) < 1 {
    flag.Usage()
    os.Exit(1)
  }

  port, err := strconv.Atoi(argv[0])
  if err != nil || port < 0 || port > 65535 {
    lib.Fatal(fmt.Sprintf("Port %s bad", argv[0]))
  }
  port_str := fmt.Sprintf(":%d", port)

  timeout := lib.Indefinitely
  if len(argv) >= 2 {
    timeout, err = strconv.ParseFloat(argv[1], 64)
    if err != nil || timeout < 0 {
      lib.Fatal(fmt.Sprintf("Timeout %s bad", argv[1]))
    }
  }

  var result bool

  if udp {
    port_spec := fmt.Sprintf("UDP%s", port_str)
    result = lib.Run(
      func() { // before
        if os.Getuid() != 0 {
          lib.Fatal("Must be root to wait for local UDP server socket to open (due to use of lsof)")
        }
        if exec.Command("which", "lsof").Run() != nil {
          lib.Fatal("lsof unavailable (required to wait for local UDP server sockets)")
        }
      },
      func() error { // each
        return exec.Command("lsof", "-n", "-i", port_spec).Run()
      },
      timeout)
  } else { /* tcp */

    var proto string
    if ip4 && !ip6 {
      proto = "tcp4"
    } else if !ip4 && ip6 {
      proto = "tcp6"
    } else {
      proto = "tcp"
    }

    result = lib.Run(nil, /* before */
      func() error { // each
        _, err := net.Dial(proto, port_str)
        return err
      },
      timeout)
  }

  if !result {
    lib.Fatal("Timeout")
  }
}
